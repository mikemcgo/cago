package cmd // import "electric-it.io/cago/cmd"

import (
	"io"

	"github.com/apex/log"
	"github.com/spf13/cobra"
)

const (
	debugFlagLong        = "debug"
	debugFlagShort       = "d"
	debugFlagDescription = "Enable debug output"
	debugFlagDefault     = false

	versionFlagLong        = "version"
	versionFlagShort       = "v"
	versionFlagDescription = "Print the Cago version"
	versionFlagDefault     = false
)

var (
	debugFlag = debugFlagDefault

	versionFlag = versionFlagDefault
)

// RootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:               "cago",
	Short:             "Cagophilist (cago for short) helps manage AWS profiles that are linked to a SAML identity provider",
	Long:              ``,
	PersistentPreRunE: persistentPreRunE,
	SilenceUsage:      true,
	RunE: func(cmd *cobra.Command, args []string) error {
		cmd.Printf("Cago version: %s\n", Version)

		return cmd.Usage()
	},
}

func persistentPreRunE(cmd *cobra.Command, args []string) error {
	if debugFlag {
		cmd.Printf("Cago version: %s\n", Version)

		log.SetLevel(log.DebugLevel)

		cmd.Println("Debug logging enabled!")
	}

	return nil
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() error {
	executeError := rootCmd.Execute()
	if executeError != nil {
		return executeError
	}

	return nil
}

// ExecuteForTesting is useful for testing purposes
func ExecuteForTesting(args []string, output io.Writer) error {
	rootCmd.SetArgs(args)
	rootCmd.SetOutput(output)

	return Execute()
}

func init() {
	rootCmd.PersistentFlags().BoolVarP(&debugFlag, debugFlagLong, debugFlagShort, debugFlagDefault, debugFlagDescription)

	rootCmd.Flags().BoolVarP(&versionFlag, versionFlagLong, versionFlagShort, versionFlagDefault, versionFlagDescription)
}
