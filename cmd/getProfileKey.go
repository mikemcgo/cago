package cmd // import "electric-it.io/cago/cmd"

import (
	"fmt"

	"github.com/apex/log"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"

	"electric-it.io/cago/aws"
)

// getAccessKeyIdCmd represents the getAccessKeyId command
var getProfileKeyCmd = &cobra.Command{
	Use:   "get-profile-key",
	Short: "Returns the value of the specified key from the specified profile",
	Long:  ``,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) != 2 {
			log.Errorf("You must provide a profile and a key!")
			return errors.New("Failed to get profile key")
		}
		profileName := args[0]
		keyName := args[1]

		log.Debugf("Getting key (%s) from profile (%s)", keyName, profileName)

		keyValue, getKeyValueError := aws.GetKeyValue(profileName, keyName)
		if getKeyValueError != nil {
			log.Errorf("Unable to find key (%s) in profile (%s): %s", keyName, profileName, getKeyValueError)
			return getKeyValueError
		}

		// This must be output to stdout
		fmt.Println(keyValue)

		return nil
	},
}

func init() {
	rootCmd.AddCommand(getProfileKeyCmd)
}
