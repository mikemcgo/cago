package aws_test // import "electric-it.io/cago/aws"

import (
	"testing"

	"github.com/go-errors/errors"

	"electric-it.io/cago/aws"
)

func TestParseArnSad(t *testing.T) {
	{
		_, parseARNError := aws.ParseARN("")
		if parseARNError == nil {
			t.Errorf("Expected error when asked to parse empty string")
		}
	}

	{
		_, parseARNError := aws.ParseARN("this:is:not:a:valid:arn")
		if parseARNError == nil {
			t.Errorf("Expected error when asked to parse invalid arn")
		}
	}
}

func TestParseArnHappy(t *testing.T) {
	var goodARN = "arn:aws:this:is:valid:arn"

	{
		arn, parseARNError := aws.ParseARN(goodARN)
		if parseARNError != nil {
			t.Errorf("Error trying to parse arn:\n%s", parseARNError.(*errors.Error).ErrorStack())
		}

		arnAsString := arn.String()
		if arnAsString != goodARN {
			t.Errorf("got %s; expected %s", arnAsString, goodARN)
		}
	}
}
