package aws // import "electric-it.io/cago/aws"

import (
	"github.com/apex/log"
	"github.com/aws/aws-sdk-go/service/sts"
	"github.com/aws/aws-sdk-go/service/sts/stsiface"
)

var (
	// MockAssumeRoleWithSAMLOutput will be returned by AssumeRoleWithSAML
	MockAssumeRoleWithSAMLOutput *sts.AssumeRoleWithSAMLOutput

	// MockAssumeRoleWithSAMLError will be returned by AssumeRoleWithSAML
	MockAssumeRoleWithSAMLError error
)

// MockSTS implements the STS API interface for mocking the service during testing
type MockSTS struct {
	stsiface.STSAPI
}

func NewMockSTS() *MockSTS {
	return &MockSTS{}
}

func (*MockSTS) AssumeRoleWithSAML(*sts.AssumeRoleWithSAMLInput) (*sts.AssumeRoleWithSAMLOutput, error) {
	log.Debugf("MockSTS Activated!\nOutput:\n%+v\nError:\n%+v", MockAssumeRoleWithSAMLOutput, MockAssumeRoleWithSAMLError)

	return MockAssumeRoleWithSAMLOutput, MockAssumeRoleWithSAMLError
}
