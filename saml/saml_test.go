package saml_test // import "electric-it.io/cago/saml"

import (
	"encoding/base64"
	"io/ioutil"
	"os"
	"testing"

	"github.com/spf13/viper"
	"github.com/zalando/go-keyring"

	"gopkg.in/jarcoal/httpmock.v1"

	"electric-it.io/cago/saml"
)

const (
	FAKESAMLENDPOINT = "https://bogusurl.local"
)

func init() {
	viper.Set("TESTMODE", true)

	os.Unsetenv("HTTP_PROXY")
	os.Unsetenv("http_proxy")

	// Be sure the credentials cache is using the mock storage
	keyring.MockInit()

	viper.Set("AuthenticationUrl", FAKESAMLENDPOINT)
}

func TestGetSAMLAssertionBase64_Happy(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	testFile := "testdata/authenticationPostReturn.html"
	fileAsBytes, readError := ioutil.ReadFile(testFile)
	if readError != nil {
		t.Errorf("Unable to read test file (%s): %+v", testFile, readError)
	}

	httpmock.RegisterResponder("POST", FAKESAMLENDPOINT, httpmock.NewStringResponder(200, string(fileAsBytes)))

	samlAssertionBase64, error := saml.GetSAMLAssertionBase64("test", "banana")
	if error != nil {
		t.Fatalf("Error getting Base64 SAML assertion: %+v", error)
	}

	if samlAssertionBase64 == "" {
		t.Fatalf("unexpectedly receieved blank SAML assertion")
	}
}

func TestGetSAMLAssertionBase64_Sad_BadStatus(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("POST", FAKESAMLENDPOINT, httpmock.NewStringResponder(500, ""))

	_, error := saml.GetSAMLAssertionBase64("test", "banana")
	if error == nil {
		t.Fatalf("Expected to get an error!")
	}
}

func TestGetSAMLAssertionBase64_NullResponseBody(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("POST", FAKESAMLENDPOINT, httpmock.NewStringResponder(200, ""))

	_, error := saml.GetSAMLAssertionBase64("test", "banana")
	if error == nil {
		t.Fatalf("Expected to get an error!")
	}
}

func TestGetSAMLAssertionBase64_MissingSAML(t *testing.T) {
	testFile := "testdata/authenticationPostReturnMissingSAML.html"
	fileAsBytes, readError := ioutil.ReadFile(testFile)
	if readError != nil {
		t.Errorf("Unable to read test file (%s): %+v", testFile, readError)
	}

	httpmock.RegisterResponder("POST", FAKESAMLENDPOINT, httpmock.NewStringResponder(200, string(fileAsBytes)))

	_, error := saml.GetSAMLAssertionBase64("test", "banana")
	if error == nil {
		t.Fatalf("Expected to get an error!")
	}
}

func TestParseAWSRoles_Sad_Simple(t *testing.T) {
	{
		_, getAuthorizedRolesError := saml.GetAuthorizedRoles("")
		if getAuthorizedRolesError == nil {
			t.Errorf("Expected error parsing empty string")
		}
	}

	{
		_, getAuthorizedRolesError := saml.GetAuthorizedRoles("junk")
		if getAuthorizedRolesError == nil {
			t.Errorf("Expected error parsing junk string")
		}
	}

	{
		_, getAuthorizedRolesError := saml.GetAuthorizedRoles("anVua3N0cmluZw=")
		if getAuthorizedRolesError == nil {
			t.Errorf("Expected error parsing invalid Base64 string")
		}
	}

	{
		_, getAuthorizedRolesError := saml.GetAuthorizedRoles("anVua3N0cmluZw==")
		if getAuthorizedRolesError == nil {
			t.Errorf("Expected error parsing junk Base64 string")
		}
	}
}

func TestParseAWSRoles_Sad_Complex(t *testing.T) {
	{
		samlAssertion, readFileError := ioutil.ReadFile("testdata/badSAMLAssertion.xml")
		if readFileError != nil {
			t.Fatalf("Unable to read test file: %s", readFileError)
		}

		encodedSAMLAssertion := base64.StdEncoding.EncodeToString(samlAssertion)

		_, getAuthorizedRolesError := saml.GetAuthorizedRoles(encodedSAMLAssertion)
		if getAuthorizedRolesError == nil {
			t.Errorf("Expected error parsing bad SAML assertion")
		}
	}

	{
		samlAssertion, readFileError := ioutil.ReadFile("testdata/goodSAMLAssertion.xml")
		if readFileError != nil {
			t.Fatalf("Unable to read test file: %s", readFileError)
		}

		encodedSAMLAssertion := base64.StdEncoding.EncodeToString(samlAssertion)

		authorizedRoles, getAuthorizedRolesError := saml.GetAuthorizedRoles(encodedSAMLAssertion)
		if getAuthorizedRolesError != nil {
			t.Fatalf("Error retrieving roles from SAML assertion\n%+v", getAuthorizedRolesError)
		}

		if len(authorizedRoles) != 2 {
			t.Fatalf("Expected 2; got %d", len(authorizedRoles))
		}

		role1ARN := "arn:aws:iam::123456789012:role/role1"
		if authorizedRoles[0].RoleARN != role1ARN {
			t.Fatalf("Expected %s; got %s", role1ARN, authorizedRoles[0].RoleARN)
		}

		principal1ARN := "arn:aws:iam::123456789012:saml-provider/saml-provider"
		if authorizedRoles[0].PrincipalARN != principal1ARN {
			t.Fatalf("Expected %s; got %s", principal1ARN, authorizedRoles[0].PrincipalARN)
		}

		role2ARN := "arn:aws:iam::123456789012:role/role2"
		if authorizedRoles[1].RoleARN != role2ARN {
			t.Errorf("Expected %s; got %s", role2ARN, authorizedRoles[0].RoleARN)
		}

		principal2ARN := "arn:aws:iam::123456789012:saml-provider/saml-provider"
		if authorizedRoles[1].PrincipalARN != principal2ARN {
			t.Errorf("Expected %s; got %s", principal2ARN, authorizedRoles[0].PrincipalARN)
		}
	}
}
